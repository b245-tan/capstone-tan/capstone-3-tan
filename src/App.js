import './App.css';

import { useState, useEffect } from 'react';
import {BrowserRouter as Router, Routes, Route} from 'react-router-dom'


import Home from './pages/Home.js'
import Login from './pages/Login.js'
import Navbar from './components/NavBar/AppNavBar.js'
import Sidebar from './components/SideBar/AppSideBar.js'
import Footer from './components/Footer/Footer.js'
import Register from './pages/Register.js'
import Logout from './pages/Logout.js'
import Products from './pages/Products.js'
import NewArrivals from "./pages/NewArrivals.js"
import ProductView from './components/Products/ProductView.js'
import MyCart from './pages/MyCart.js'
import AdminDashboard from './pages/AdminDashboard.js'
import AddNewProduct from './components/AdminDashboard/AddNewProduct.js'
import ShowAllProducts from './components/AdminDashboard/ShowAllProducts.js'
import NotFound from './pages/NotFound.js'
import {UserProvider} from './UserContext.js'


function App() {


  const [user, setUser] = useState(null);

  useEffect(() => {
    // console.log(user)
  }, [user]);

  const unSetUser = () => {
    localStorage.clear();
  }

  useEffect(() => {
      fetch(`${process.env.REACT_APP_API_URL}/user/details`, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      })
      .then(result => result.json())
      .then(data => {
        // console.log(data)

        if(localStorage.getItem('token') !== null) {
            setUser({
              id: data._id,
              isAdmin: data.isAdmin
            })
        } else {
          setUser(null);
        }
        
      })
  }, [])


  const [isOpen, setIsOpen] = useState(false)

  const toggle = () => {
    setIsOpen(!isOpen);
  }



  return (
 
    <UserProvider value = {{user, setUser, unSetUser}}>
    <Router>
      <Sidebar isOpen={isOpen} toggle={toggle} />
      <Navbar toggle={toggle}/>

      <Routes>
        <Route path = "/" element = {<Home/>}/>
        <Route path = "/login" element = {<Login/>}/>
        <Route path = "/register" element = {<Register/>}/>
        <Route path = "/logout" element = {<Logout/>}/>
        <Route path = "/catalogue" element = {<Products/>}/>
        <Route path = "/newarrivals" element = {<NewArrivals/>}/>
        <Route path = "/product/:productId" element={<ProductView />}/>
        <Route path = "/cart" element = {<MyCart/>}/>
        <Route path = "/AdminDashboard" element = {<AdminDashboard/>}/>
        <Route path = "/AdminDashboard/AddNewProduct" element = {<AddNewProduct/>}/>
        <Route path = "/AdminDashboard/ShowAllProducts" element = {<ShowAllProducts/>}/>
        <Route path='*' element={<NotFound />}/>
      </Routes>

      <Footer />

    </Router>
    </UserProvider>
 
  );
}

export default App;
