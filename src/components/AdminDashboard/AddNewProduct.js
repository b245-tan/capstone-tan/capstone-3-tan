import {Button, Form, Container, Row, Col} from 'react-bootstrap';
import { useEffect, useState, useContext } from "react";
import {Link} from "react-router-dom"
import Swal from 'sweetalert2'
import {Navigate, useNavigate} from "react-router-dom"

function AddNewProduct() {
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [isActive, setIsActive] = useState(false)
	const [price, setPrice] = useState("");

	const navigate = useNavigate();

	useEffect(() => {
		if(name !== "" && description !== "" && price !== ""){
			setIsActive(true)
		}
		else {
			setIsActive(false)
		}
	}, [name, description, price])



	function addProduct(event){
		event.preventDefault()
		fetch(`${process.env.REACT_APP_API_URL}/product/add`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price
			})
		})
		.then(result => result.json())
		.then(data => {
			if(data === true) {
				Swal.fire({
					title: "Product Added Successfully!",
					icon: "success",
					text: "Product Added to Catalogue",
					showConfirmButton: true,
					timer: 2000
				})
				setName('')
				setDescription('')
				setPrice('')
				navigate("/catalogue")
			}
			else{
				Swal.fire({
					title: "Product Creation Failed!",
					icon: "error",
					text: "Try again!",
					showConfirmButton: false,
					timer: 2000
				})
			}
		})
	}


	return(
		<div className= "pt-5 mt-5 pb-5 mb-5">
			<Container className = "fluid mx-auto col-md-8">
				<h1 className = "text-center">Admin Dashboard</h1>
				<div className="mx-auto text-center">
				<Button as = {Link} to = "/AdminDashboard/AddNewProduct" variant="primary" className="me-2">Add Product</Button>
				{' '}
				<Button as = {Link} to = "/AdminDashboard/ShowAllProducts" variant="secondary" className="ms-2">Show All Products</Button>
				</div>
				<h3 className = "text-left mt-2 mb-3">Add Product</h3>


				<Row>
					<Col className = "col-md-8">
					<Form className = 'mt-5 mx-5' onSubmit = {event=> addProduct(event)}>
					<Form.Group className="mb-3" controlId="formProductName">
				<Form.Label>Product Name</Form.Label>
				<Form.Control 
					type="text" 
					placeholder="Product Name" 
					value ={name}
					onChange = {event => setName(event.target.value)}
					required
					/>
				<Form.Text className="text-muted">
				</Form.Text>
				</Form.Group>

				<Form.Group className="mb-3" controlId="formProductDescription">
				<Form.Label>Product Description</Form.Label>
				<Form.Control 
					className = "mb-2"
					type="textarea" 
					placeholder="Product Description" 
					value ={description}
					onChange = {event => setDescription(event.target.value)}
					required
				/>
				</Form.Group>

				<Form.Group className="mb-3" controlId="formProductPrice">
				<Form.Label>Product Price</Form.Label>
				<Form.Control 
					className = "mb-2"
					type="number" 
					placeholder="Product Price" 
					value ={price}
					onChange = {event => setPrice(event.target.value)}
					required
				/>
				</Form.Group>
				{
				isActive ?
				<Button variant="primary" type="submit">
				Submit
				</Button>
				:
				<Button variant="secondary" type="submit" disabled>
				Submit
				</Button>
				      
				}
						</Form>
					</Col>
				</Row>
			</Container>
		</div>
	)
}

export default AddNewProduct;