import React, {useState, useEffect, useContext} from 'react'
import Modal from 'react-bootstrap/Modal';
import Swal from 'sweetalert2'
import {Navigate, useNavigate, Link} from "react-router-dom"
import {Button, Form, Container, Row, Col} from 'react-bootstrap';

// import ModalToggle from "./EditProductModal.js"
// import ModalToggle from "./ModalToggle.js"
import EditProductModal from "./EditProductModal.js"
import deletebutton from '../../images/deletebutton_3.png'
import addbutton from '../../images/addbutton.png'
import editbutton from '../../images/editbutton.png'

export default function AllProductsList({productProp}){

	// console.log(data)

	const {_id, name, description, price, isActive, createdOn, __v} = productProp

	const [isNewActive, setNewIsActive] = useState(false)
	// setNewIsActive(isActive)

	useEffect(() => {

		setNewIsActive(isActive)

	}, [])


	const navigate = useNavigate()
	const [newName, setnewName] = useState(name);
	const [newDescription, setnewDescription] = useState(description);
	const [newPrice, setnewPrice] = useState(price);

	const [modalShow, setModalShow] = React.useState(false);

	const [show, setShow] = useState(false);
	const handleClose = () => setShow(false);
	const handleShow = () => setShow(true);


	function archiveProduct(_id) {
		fetch(`${process.env.REACT_APP_API_URL}/product/archive/${_id}`, {
			method: 'GET',
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(result => result.json())
		.then(data => {
			if(data === true) {
			const Toast = Swal.mixin({
			  toast: true,
			  position: 'top-end',
			  showConfirmButton: false,
			  timer: 3000,
			  timerProgressBar: true
			})

			Toast.fire({
			  icon: 'success',
			  title: 'Product Successfuly Archived!'
			})
			setNewIsActive(false)

			} else {
				const Toast = Swal.mixin({
				  toast: true,
				  position: 'top-end',
				  showConfirmButton: false,
				  timer: 3000,
				  timerProgressBar: true
				})

				Toast.fire({
				  icon: 'error',
				  title: 'Action was Not Successful!'
				})
			}
			
		})
	}


	function activateProduct(_id) {
		fetch(`${process.env.REACT_APP_API_URL}/product/activate/${_id}`, {
			method: 'GET',
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(result => result.json())
		.then(data => {
			if(data === true) {
			const Toast = Swal.mixin({
			  toast: true,
			  position: 'top-end',
			  showConfirmButton: false,
			  timer: 3000,
			  timerProgressBar: true
			})

			Toast.fire({
			  icon: 'success',
			  title: 'Product Successfuly Added!'
			})
			setNewIsActive(true)

			} else {
				const Toast = Swal.mixin({
				  toast: true,
				  position: 'top-end',
				  showConfirmButton: false,
				  timer: 3000,
				  timerProgressBar: true
				})

				Toast.fire({
				  icon: 'error',
				  title: 'Action was Not Successful!'
				})
			}
		})
	}


	function editProduct(_id) {
	    fetch(`${process.env.REACT_APP_API_URL}/product/update/${_id}`, {
	      method: 'PUT',
	      headers: {
	        'Content-Type': 'application/json',
	        Authorization: `Bearer ${localStorage.getItem('token')}`
	      },
	      body: JSON.stringify({
	        name: newName,
	        description: newDescription,
	        price: newPrice
	      })
	    })
	    .then(result => result.json())
	    .then(data => {
	      if(data === true) {
	      const Toast = Swal.mixin({
	        toast: true,
	        position: 'top-end',
	        showConfirmButton: false,
	        timer: 3000,
	        timerProgressBar: true
	      })

	      Toast.fire({
	        icon: 'success',
	        title: 'Product Updated!'
	      })
	      // navigate("/AdminDashboard/ShowAllProducts")
	      setnewName("")
	      setnewDescription("")
	      setnewPrice("")
	      handleClose()

	      } else {
	        const Toast = Swal.mixin({
	          toast: true,
	          position: 'top-end',
	          showConfirmButton: false,
	          timer: 3000,
	          timerProgressBar: true
	        })

	        Toast.fire({
	          icon: 'error',
	          title: 'Update Failed!'
	        })
	      }
	    })
	  }


	/*function EditProductModal(props) {


	  return (
	    <Modal
	      {...props}
	      size="lg"
	      aria-labelledby="contained-modal-title-vcenter"
	      centered
	    >
	      <Modal.Header closeButton>
	        <Modal.Title id="contained-modal-title-vcenter">
	          Edit Product
	        </Modal.Title>
	      </Modal.Header>
	      <Modal.Body>
	        <h3>Input Changes</h3>
	        <Row>
	          <Col className = "col-md-8">
	          <Form className = 'mt-2 mx-3'>
	          <Form.Group className="mb-3" controlId="formEditProductName">
	        <Form.Label>Product Name</Form.Label>
	        <Form.Control 
	          type="text" 
	          placeholder="Product Name" 
	          value ={newName}
	          onChange = {props => setnewName(props.target.value)}
	          required
	          />
	        <Form.Text className="text-muted">
	        </Form.Text>
	        </Form.Group>

	        <Form.Group className="mb-3" controlId="formEditProductDescription">
	        <Form.Label>Product Description</Form.Label>
	        <Form.Control 
	          className = "mb-2"
	          type="text" 
	          placeholder="Product Description" 
	          value ={newDescription}
	          onChange = {props => setnewDescription(props.target.value)}
	          required
	        />
	        </Form.Group>

	        <Form.Group className="mb-3" controlId="formEditProductPrice">
	        <Form.Label>Product Price</Form.Label>
	        <Form.Control 
	          className = "mb-2"
	          type="number" 
	          placeholder="Product Price" 
	          value ={newPrice}
	          onChange = {props => setnewPrice(props.target.value)}
	          required
	        />
	        </Form.Group>
	            </Form>
	          </Col>
	        </Row>

	      </Modal.Body>
	      <Modal.Footer>
	        <Button variant="secondary" onClick={props.onHide}>Cancel</Button>
	        <Button onClick={() => editProduct(_id)}>Save Changes</Button>
	      </Modal.Footer>

	    </Modal>
	  );
	}

	function ModalToggle() {
	  
	  console.log(modalShow)

	  return (
	    <>
	      <Button variant="light" size="sm" className = "m-2 ms-1 me-1" onClick={() => setModalShow(true)}>
	        <img src = {editbutton} style={{ width: 25, height: 25 }}/>
	      </Button>

	      <EditProductModal
	        show={modalShow}
	        onHide={() => setModalShow(false)}
	      />
	    </>
	  );
	}*/

	



		function ModalToggle() {


		  return (
		    <>
		      <Button variant="light" size="sm" className = "m-2 ms-1 me-1" onClick={() => setShow(true)}>
	        <img src = {editbutton} style={{ width: 25, height: 25 }}/>
	      </Button>

		      <Modal
		        show={show}
		        onHide={handleClose}
		        size="lg"
	      		aria-labelledby="contained-modal-title-vcenter"
	      		centered
		      >
		        <Modal.Header closeButton>
		          <Modal.Title>Edit Product</Modal.Title>
		        </Modal.Header>
		        	<Modal.Body>
		        	  <h3>Input Changes</h3>
		        	  <Row>
		        	    <Col className = "col-md-8">
		        	    <Form className = 'mt-2 mx-3'>
		        	    <Form.Group className="mb-3" controlId="formEditProductName">
		        	  <Form.Label>Product Name</Form.Label>
		        	  <Form.Control 
		        	    type="text" 
		        	    placeholder="Product Name" 
		        	    value ={newName}
		        	    onChange = {e => setnewName(e.target.value)}
		        	    required
		        	    />
		        	  <Form.Text className="text-muted">
		        	  </Form.Text>
		        	  </Form.Group>

		        	  <Form.Group className="mb-3" controlId="formEditProductDescription">
		        	  <Form.Label>Product Description</Form.Label>
		        	  <Form.Control 
		        	    className = "mb-2"
		        	    type="text" 
		        	    placeholder="Product Description" 
		        	    value ={newDescription}
		        	    onChange = {e=> setnewDescription(e.target.value)}
		        	    required
		        	  />
		        	  </Form.Group>

		        	  <Form.Group className="mb-3" controlId="formEditProductPrice">
		        	  <Form.Label>Product Price</Form.Label>
		        	  <Form.Control 
		        	    className = "mb-2"
		        	    type="number" 
		        	    placeholder="Product Price" 
		        	    value ={newPrice}
		        	    onChange = {e => setnewPrice(e.target.value)}
		        	    required
		        	  />
		        	  </Form.Group>
		        	      </Form>
		        	    </Col>
		        	  </Row>

		        	</Modal.Body>
		        <Modal.Footer>
		          <Button variant="secondary" onClick={handleClose}>
		            Close
		          </Button>
		          <Button variant="primary" onClick={() => editProduct(_id)}>Save Changes</Button>
		        </Modal.Footer>
		      </Modal>
		    </>
		  );
		}







	return(

		<tr>
			<td className = "text-center">{name}</td>
			<td>{description}</td>
			<td className = "text-center">{price}</td>
			<td>{isNewActive.toString()}</td>

			{
			isNewActive ?
				<td className = "text-center my-auto">
				{<ModalToggle/ >}
				<Button variant="light" size="sm" className = "m-2" onClick={() => archiveProduct(_id)}>
				<img src = {deletebutton} style={{ width: 25, height: 25 }}/>
				</Button>
				</td>
			:
				<td className = "text-center my-auto">
				{<ModalToggle/ >}
				<Button variant="light" size="sm" className = "m-2 ms-2" onClick={() => activateProduct(_id)}>
				<img src = {addbutton} style={{ width: 25, height: 25 }}/>
				</Button>
				</td>
			}
		</tr>
	)
}