import { useEffect, useState, useContext } from "react";


export default function CartItems({cartProp}){

	const {productId, productName, size, quantity, price} = cartProp

	return(
		<ul>
			{<div>Product Name: {productName}</div>}
			{<div>Product Size: {size}</div>}
			{<div>Product Quantity: {quantity}</div>}
			{<div>Price: <strong>₱{price}</strong></div>}
		</ul>
	)
		
}