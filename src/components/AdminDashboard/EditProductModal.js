import React from 'react'
import Modal from 'react-bootstrap/Modal';
import {Button, Form, Container, Row, Col} from 'react-bootstrap';
import { useEffect, useState, useContext } from "react";
import Swal from 'sweetalert2'
import {Navigate, useNavigate, Link} from "react-router-dom"

import AllProductsList from "./AllProductsList.js"
import editbutton from '../../images/editbutton.png'
// import { productProp } from "./AllProductsList.js"

function EditProductModal(props) {

  // console.log(props)

  const [newName, setnewName] = useState("");
  const [newDescription, setnewDescription] = useState("");
  const [newPrice, setnewPrice] = useState("");



  /*function editProduct(_id) {
    fetch(`${process.env.REACT_APP_API_URL}/product/update/${_id}`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify({
        name: newName,
        description: newDescription,
        price: newPrice
      })
    })
    .then(result => result.json())
    .then(data => {
      if(data === true) {
      const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true
      })

      Toast.fire({
        icon: 'success',
        title: 'Product Updated!'
      })
      navigate("/AdminDashboard/ShowAllProducts")

      } else {
        const Toast = Swal.mixin({
          toast: true,
          position: 'top-end',
          showConfirmButton: false,
          timer: 3000,
          timerProgressBar: true
        })

        Toast.fire({
          icon: 'error',
          title: 'Update Failed!'
        })
      }
    })
  }*/

  return (
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Edit Product
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <h3>Input Changes</h3>
        <Row>
          <Col className = "col-md-8">
          <Form className = 'mt-2 mx-3'>
          <Form.Group className="mb-3" controlId="formEditProductName">
        <Form.Label>Product Name</Form.Label>
        <Form.Control 
          type="text" 
          placeholder="Product Name" 
          value ={newName}
          onChange = {(props) => setnewName(props.target.value)}
          required
          />
        <Form.Text className="text-muted">
        </Form.Text>
        </Form.Group>

        <Form.Group className="mb-3" controlId="formEditProductDescription">
        <Form.Label>Product Description</Form.Label>
        <Form.Control 
          className = "mb-2"
          type="text" 
          placeholder="Product Description" 
          value ={newDescription}
          onChange = {props => setnewDescription(props.target.value)}
          required
        />
        </Form.Group>

        <Form.Group className="mb-3" controlId="formEditProductPrice">
        <Form.Label>Product Price</Form.Label>
        <Form.Control 
          className = "mb-2"
          type="number" 
          placeholder="Product Price" 
          value ={newPrice}
          onChange = {props => setnewPrice(props.target.value)}
          required
        />
        </Form.Group>
            </Form>
          </Col>
        </Row>

      </Modal.Body>
      <Modal.Footer>
        <Button variant="secondary" onClick={props.onHide}>Cancel</Button>
        <Button onClick={props.onHide}>Save Changes</Button>
      </Modal.Footer>

    </Modal>
  );
}

function ModalToggle() {
  const [modalShow, setModalShow] = React.useState(false);

  return (
    <>
      <Button variant="light" size="sm" className = "m-2 ms-1 me-1" onClick={() => setModalShow(true)}>
        <img src = {editbutton} style={{ width: 25, height: 25 }}/>
      </Button>

      <EditProductModal
        show={modalShow}
        onHide={() => setModalShow(false)}
      />
    </>
  );
}

export default ModalToggle