import React, {useState, useEffect, useContext} from 'react'
import Modal from 'react-bootstrap/Modal';
import Swal from 'sweetalert2'
import {Navigate, useNavigate, Link} from "react-router-dom"
import {Button, Form, Container, Row, Col} from 'react-bootstrap';


import EditProductModal from "./EditProductModal.js"
import deletebutton from '../../images/deletebutton_3.png'
import addbutton from '../../images/addbutton.png'
import editbutton from '../../images/editbutton.png'
import AllProductsList from './AllProductsList.js'


export default function ModalToggle() {

	// const {_id, name, description, price, isActive, createdOn, __v} = order

	const [formValues, setFormValues] = useState({
		newName: "",
		newDescription: "",
		newPrice: ""
	})

	const handleChange = (event) => {
        const { name, value } = event.target;

        setFormValues({ ...formValues, [name]: value });
    };

/*	const [newName, setnewName] = useState("");
	const [newDescription, setnewDescription] = useState("");
	const [newPrice, setnewPrice] = useState("");*/

	const [show, setShow] = useState(false);
	const handleClose = () => setShow(false);
	const handleShow = () => setShow(true);



	/*function archiveProduct(_id) {
		fetch(`${process.env.REACT_APP_API_URL}/product/archive/${_id}`, {
			method: 'GET',
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(result => result.json())
		.then(data => {
			if(data === true) {
			const Toast = Swal.mixin({
			  toast: true,
			  position: 'top-end',
			  showConfirmButton: false,
			  timer: 3000,
			  timerProgressBar: true
			})

			Toast.fire({
			  icon: 'success',
			  title: 'Product Successfuly Archived!'
			})
			navigate("/AdminDashboard/AddNewProduct")
			navigate("/AdminDashboard/ShowAllProducts")
			} else {
				const Toast = Swal.mixin({
				  toast: true,
				  position: 'top-end',
				  showConfirmButton: false,
				  timer: 3000,
				  timerProgressBar: true
				})

				Toast.fire({
				  icon: 'error',
				  title: 'Action was Not Successful!'
				})
			}
			
		})
	}*/


		  return (
		    <>
		      <Button variant="light" size="sm" className = "m-2 ms-1 me-1" onClick={() => setShow(true)}>
	        <img src = {editbutton} style={{ width: 25, height: 25 }}/>
	      </Button>

		      <Modal
		        show={show}
		        onHide={handleClose}
		        size="lg"
	      		aria-labelledby="contained-modal-title-vcenter"
	      		centered
		      >
		        <Modal.Header closeButton>
		          <Modal.Title>Edit Product</Modal.Title>
		        </Modal.Header>
		        	<Modal.Body>
		        	  <h3>Input Changes</h3>
		        	  <Row>
		        	    <Col className = "col-md-8">
		        	    <Form className = 'mt-2 mx-3'>
		        	    <Form.Group className="mb-3" controlId="formEditProductName">
		        	  <Form.Label>Product Name</Form.Label>
		        	  <Form.Control 
		        	    type="text" 
		        	    placeholder="Product Name" 
		        	    value ={formValues.newName}
		        	    onChange = {handleChange}
		        	    required
		        	    />
		        	  <Form.Text className="text-muted">
		        	  </Form.Text>
		        	  </Form.Group>

		        	  <Form.Group className="mb-3" controlId="formEditProductDescription">
		        	  <Form.Label>Product Description</Form.Label>
		        	  <Form.Control 
		        	    className = "mb-2"
		        	    type="text" 
		        	    placeholder="Product Description" 
		        	    value ={formValues.newDescription}
		        	    onChange = {handleChange}
		        	    required
		        	  />
		        	  </Form.Group>

		        	  <Form.Group className="mb-3" controlId="formEditProductPrice">
		        	  <Form.Label>Product Price</Form.Label>
		        	  <Form.Control 
		        	    className = "mb-2"
		        	    type="number" 
		        	    placeholder="Product Price" 
		        	    value ={formValues.newPrice}
		        	    onChange = {handleChange}
		        	    required
		        	  />
		        	  </Form.Group>
		        	      </Form>
		        	  <AllProductsList data = {formValues}/>
		        	    </Col>
		        	  </Row>

		        	</Modal.Body>
		        <Modal.Footer>
		          <Button variant="secondary" onClick={handleClose}>
		            Close
		          </Button>
		          <Button variant="primary">Save Changes</Button>
		        </Modal.Footer>
		      </Modal>
		    </>
		 )
}