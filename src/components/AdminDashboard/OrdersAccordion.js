import Accordion from 'react-bootstrap/Accordion';
import { useEffect, useState, useContext } from "react";

import CartItems from "./CartItems.js"

export default function OrdersAccordion({allOrderProp}){

	const {_id, userId, cart, totalAmount, AddedOn, version} = allOrderProp
	
	const [isThereOrder, setIsThereOrder] = useState(false);

	const [cartItems, setCartItems] = useState([])

	useEffect(() => {
		if(totalAmount !== 0){
		setIsThereOrder(true)
		} else {
		setIsThereOrder(false)
	}
	}, [totalAmount])

	let key = Math.random() + totalAmount;	



	useEffect(() =>{
	
		setCartItems(cart.map(order => {
			return(
				<CartItems key = {order._id} cartProp = {order}/>
			)
		}))

	}, [])


	

	return(
			isThereOrder ?
			<Accordion.Item eventKey={key} className="mt-3 mb-3">
				<Accordion.Header>Orders for user: <span className="text-warning"> {userId}</span></Accordion.Header>
				<Accordion.Body content="{allOrderProp}">
				<div className = "mb-3">Created On: {AddedOn}</div>

					{cartItems}
	
				<span>Total:</span> <span className="text-warning">₱ {totalAmount}</span>
				</Accordion.Body>
			</Accordion.Item>
			:
			""
	)
	
}