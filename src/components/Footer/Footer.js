import React from 'react'
import {FaFacebook, FaInstagram, FaYoutube, FaTwitter, FaLinkedin} from 'react-icons/fa'
import {FooterContainer, FooterWrap, FooterLinksContainer, FooterLinksWrapper, FooterLinkItems, FooterLinkTitle, FooterLink, SocialMedia, SocialMediaWrap, SocialLogo, WebsiteRights, SocialIcons, SocialIconLink} from './FooterElements.js'


const Footer = () => {
	return(
		<FooterContainer>
			<FooterWrap>
				<FooterLinksContainer>
					<FooterLinksWrapper>
						<FooterLinkItems>
							<FooterLinkTitle>About Us</FooterLinkTitle>
								<FooterLink to ="/register">How it works</FooterLink>
								<FooterLink to ="/register">Testimonials</FooterLink>
								<FooterLink to ="/register">Careers</FooterLink>
								<FooterLink to ="/register">Investors</FooterLink>
								<FooterLink to ="/register">Terms of Service</FooterLink>
						</FooterLinkItems>
						<FooterLinkItems>
							<FooterLinkTitle>Contact Us</FooterLinkTitle>
								<FooterLink to ="/register">Contact</FooterLink>
								<FooterLink to ="/register">Support</FooterLink>
								<FooterLink to ="/register">Destinations</FooterLink>
								<FooterLink to ="/register">Sponsors</FooterLink>
						</FooterLinkItems>
					</FooterLinksWrapper>
					<FooterLinksWrapper>
						<FooterLinkItems>
							<FooterLinkTitle>Videos</FooterLinkTitle>
								<FooterLink to ="/register">Submit Video</FooterLink>
								<FooterLink to ="/register">Ambassadors</FooterLink>
								<FooterLink to ="/register">Agency</FooterLink>
								<FooterLink to ="/register">Influencers</FooterLink>
						</FooterLinkItems>
						<FooterLinkItems>
							<FooterLinkTitle>Social Media</FooterLinkTitle>
								<FooterLink to ="/register">Instagram</FooterLink>
								<FooterLink to ="/register">Facebook</FooterLink>
								<FooterLink to ="/register">Youtube</FooterLink>
								<FooterLink to ="/register">Twitter</FooterLink>
								<FooterLink to ="/register">TikTok</FooterLink>
						</FooterLinkItems>
					</FooterLinksWrapper>
				</FooterLinksContainer>
				<SocialMedia>
					<SocialMediaWrap>
						<SocialLogo>
							Clothing
						</SocialLogo>
						<WebsiteRights>Clothing © {new Date().getFullYear()} All rights reseved.</WebsiteRights>
						<SocialIcons>
							<SocialIconLink href="//www.facebook.com" target="_blank" aria-label="Facebook">
							<FaFacebook />
							</SocialIconLink>
							<SocialIconLink href="//www.instagram.com" target="_blank" aria-label="Instagram">
							<FaInstagram />
							</SocialIconLink>
							<SocialIconLink href="//www.youtube.com" target="_blank" aria-label="Youtube">
							<FaYoutube />
							</SocialIconLink>
							<SocialIconLink href="//www.twitter.com" target="_blank" aria-label="Twitter">
							<FaTwitter />
							</SocialIconLink>
							<SocialIconLink href="//www.linkedin.com" target="_blank" aria-label="Linkedin">
							<FaLinkedin />
							</SocialIconLink>
						</SocialIcons>
					</SocialMediaWrap>
				</SocialMedia>
			</FooterWrap>
		</FooterContainer>
	)
}

export default Footer