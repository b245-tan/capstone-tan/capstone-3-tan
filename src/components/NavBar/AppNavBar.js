import React, {useState, useEffect} from 'react'
import {Button} from 'react-bootstrap';
import {FaBars} from 'react-icons/fa'
import { Link, NavLink } from 'react-router-dom'
import { useContext } from 'react'
import Badge from 'react-bootstrap/Badge';



import {Nav, NavbarContainer, NavLogo, MobileIcon, NavMenu, NavItem, NavLinks, NavBtn, NavBtnLink, NavBtn2, NavBtnLink2, Nav2} from './NavBarElements.js'
import UserContext from "../../UserContext.js"


const Navbar = ({toggle}) => {
	const [scrollNav, setScrollNav] = useState(false)
	const [isUserAdmin, setIsAdmin] = useState(false)
	const {user, setUser} = useContext(UserContext);


	const changeNav = () => {
		if(window.scrollY >= 80 || window.location.pathname !== "/") {
			setScrollNav(true)
		} else {
			setScrollNav(false)
		}
	}

	useEffect(() => {
		window.addEventListener('scroll', changeNav)
	}, [])

	const changeWindowNav = () => {
		if(window.location.pathname !== "/"){
			setScrollNav(true)
		} else {
			setScrollNav(false)
		}
	}

	useEffect(() => {
		window.addEventListener('click', changeWindowNav)
	}, [])

	useEffect(() => {
		window.addEventListener('pageshow', changeWindowNav)
	}, [])

	useEffect(() => {
		window.addEventListener('load', changeWindowNav)
	}, [])

	useEffect(() => {
		window.addEventListener('pagehide', changeWindowNav)
	}, [])

	useEffect(() => {
		window.addEventListener('animationend', changeWindowNav)
	}, [])

	useEffect(() => {
		window.addEventListener('animationstart', changeWindowNav)
	}, [])


	/*const retrieveUserDetails = (token) => {


		fetch(`http://localhost:4000/user/details`, {
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		.then(result => result.json())
		.then(data => {

			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})
		})
	}*/
	

	

	return(

		<>
			{
			user ?
				<Nav2 scrollNav = {scrollNav}>	

					<NavbarContainer>
						<NavLogo as = {Link} to = "/">Clothing</NavLogo>
						<MobileIcon onClick={toggle}>
							<FaBars />
						</MobileIcon>
						<NavMenu>
							<NavItem>
								<NavLinks as = {NavLink} to = "/newarrivals">Discover<Badge bg="secondary ms-1">New Arrivals</Badge></NavLinks>
							</NavItem>
							<NavItem>
								<NavLinks as = {NavLink} to = "/catalogue">Products</NavLinks>
							</NavItem>

							{
								user ?
									user.isAdmin ?
									<NavItem>
									<NavLinks as = {NavLink} to = "/AdminDashboard" className=""><Button variant="primary">ADMIN DASHBOARD</Button></NavLinks>
									</NavItem>
									:
									<NavItem>
									<NavLinks as = {NavLink} to = "/cart">My Cart <Badge bg="secondary mx-2">0</Badge></NavLinks>
									</NavItem>
								:
								<NavItem>
								<NavLinks as = {NavLink} to = "/register">Sign Up</NavLinks>
								</NavItem>
							}

							
						</NavMenu>
						{
							user ?
							<NavBtn2>
								<NavBtnLink2 as = {NavLink} to = "/logout">Sign Out</NavBtnLink2>
							</NavBtn2>
							:
							<NavBtn>
								<NavBtnLink as = {NavLink} to = "/login">Sign In</NavBtnLink>
							</NavBtn>
						}
					</NavbarContainer>
				</Nav2>
				:
				<Nav scrollNav = {scrollNav}>	
					<NavbarContainer>
						<NavLogo as = {Link} to = "/">Clothing</NavLogo>
						<MobileIcon onClick={toggle}>
							<FaBars />
						</MobileIcon>

						<NavMenu>
							<NavItem>
								<NavLinks as = {NavLink} to = "/newarrivals">Discover<Badge bg="secondary ms-1">New Arrivals</Badge></NavLinks>
							</NavItem>
							<NavItem>
								<NavLinks as = {NavLink} to = "/catalogue">Products</NavLinks>
							</NavItem>
							{
								user ?
									user.isAdmin ?
									<NavItem>
									<NavLinks as = {NavLink} to = "/AdminDashboard" className=""><Button variant="primary">ADMIN DASHBOARD</Button></NavLinks>
									</NavItem>
									:
									<NavItem>
									<NavLinks as = {NavLink} to = "/cart">My Cart <Badge bg="secondary mx-2">0</Badge></NavLinks>
									</NavItem>
								:
								<NavItem>
								<NavLinks as = {NavLink} to = "/register">Sign Up</NavLinks>
								</NavItem>
							}
						</NavMenu>

						{
							user ?
							<NavBtn2>
								<NavBtnLink2 as = {NavLink} to = "/logout">Sign Out</NavBtnLink2>
							</NavBtn2>
							:
							<NavBtn>
								<NavBtnLink as = {NavLink} to = "/login">Sign In</NavBtnLink>
							</NavBtn>
						}
					</NavbarContainer>
				</Nav>
			}
		</>
	)
}

export default Navbar;