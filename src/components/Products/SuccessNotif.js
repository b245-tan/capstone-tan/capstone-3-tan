import Swal from 'sweetalert2'

export default function SuccessNotif() {

const Toast = Swal.mixin({
  toast: true,
  position: 'top-end',
  showConfirmButton: false,
  timer: 2200
})

Toast.fire({
  icon: 'success',
  title: 'Item Successfully Added to Cart!'
})

}