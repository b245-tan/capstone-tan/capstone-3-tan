import "./CartStyle.css";
import 'animate.css';

import OrdersRow from '../components/Orders/OrdersRow.js'

import { Container, Table, Row, Button } from "react-bootstrap";
import { useEffect, useState, useContext } from "react";
import {Link} from "react-router-dom"
import Swal from 'sweetalert2'

import UserContext from '../UserContext.js'

export default function MyCart() {
	
	const [orders, setOrders] = useState([]);
	const [totalAmount, setTotalAmount] = useState(0);
	const {user, setUser} = useContext(UserContext);
	// const [cartNumber, setCartNumber] = useState(0)

	useEffect(() =>{
		// fetch all orders of the user
		fetch(`${process.env.REACT_APP_API_URL}/order/viewCart`, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      })
		.then(result => result.json())
		.then(data => {
			
		if(data !== false){
			setOrders(data.cart.map(order => {
				setTotalAmount(data.totalAmount)
				return(
					<OrdersRow key = {order._id} orderProp = {order}/>
				)
			}))
		}

		})

	}, [])


	/*useEffect(() =>{

	setCartNumber(rows.length)	

	}, [])*/


	function checkOut(event){
		event.preventDefault()
		
		
				Swal.fire({
				  title: 'Are you sure?',
				  text: "Will proceed to payment.",
				  icon: 'warning',
				  confirmButtonText: 'Yes',
				  showCancelButton: true,
				  confirmButtonColor: '#3085d6',
				  cancelButtonColor: '#d33'
				}).then((sure) => {
				  if (sure.isConfirmed) {

				  	fetch(`${process.env.REACT_APP_API_URL}/order/checkOut`, {
				  		method: 'GET',
				  		headers: {
				  			Authorization: `Bearer ${localStorage.getItem('token')}`
				  		}
				  	})
				  	.then(result => result.json())
				  	.then(data => {
				  		if(data === true) {



				   Swal.fire({
				   		title: "Proceeding to Payment",
				   		icon: "info",
				   		showConfirmButton: true,
				   		confirmButtonText: 'Ok',
				   		showCancelButton: false,
				   		})
				   		.then((confirm) => {
				 		 		if (confirm) {
						   		Swal.fire({
									title: "Payment received! Enjoy your purchase!",
									icon: "success",
									text: "Transaction successful!",
									showConfirmButton: true,
									showClass: {popup: 'animate__animated animate__fadeInDown'},
									hideClass: {popup: 'animate__animated animate__fadeOutUp'}
									})
									setOrders([])	
				  			}
							})
				   	}
				   	else if(data !== true){
				   		Swal.fire({
				   			title: "Your cart is empty!",
				   			icon: "error",
				   			text: "Add items to your cart first!",
				   			showConfirmButton: true,
				   			timer: 2000
				   		})
				   	}
				   })
				  }
				})
	}




	const [isCart, setisCart] = useState(false);

	useEffect(() => {
		if(orders.length == 0){
			setisCart(false)
		}
		else if(orders.length != 0){
			setisCart(true)
		}
	}, [orders])

	  return (
	    <div className="productSlider pb-5 mb-5 mt-5 pt-5">
	      <Container className = "fluid mx-auto">
	        <h2 className="text-left mb-4 ps-4 pt-2">My Cart</h2>
	        {
	        isCart ?
	        <Row>
	          <div className="col-xl-9 col-xl-8 cartShow">
	            <Table bordered hover responsive="sm">
	              <thead className="text-center">
	                <tr>
	                  <th>Product</th>
	                  <th>Name</th>
	                  <th>Size</th>
	                  <th>Qty</th>
	                  <th>Price</th>
	                  <th>Action</th>
	                </tr>
	              </thead>
	              <tbody>
					{orders}
	              </tbody>
	            </Table>
	          </div>
	          <div className="col-sm-12 mx-auto col-md-6 col-xl-3 cartSum boxShadaw bg-light p-4">
	            <h5 className="text-left mb-4 pb-2">Cart Price</h5>
	            <div className="d-flex justify-content-between mb-4">
	              <h6>Subtotal :</h6>
	              <span>₱ {totalAmount}</span>
	            </div>
	            <div className="d-flex justify-content-between mb-4">
	              <h6 className="fw-normal">Delivery Fee :</h6>
	              <span>₱ 150</span>
	            </div>
	            <div className="d-flex justify-content-between fw-bold">
	              <h6>Total Price :</h6>
	              <span>₱ {totalAmount + 150}</span>
	            </div>
	            {
	            isCart ?
	            <Button onClick= {event=> checkOut(event)} variant="dark" size="md" className="mt-4 w-100">
	              Checkout
	            </Button>
	            :
	            <Button variant="dark" size="md" className="mt-4 w-100" disabled>
	              Checkout
	            </Button>
	          	}
	          </div>
	        </Row>
	        :
	        <div className=" text-center fluid">
	        <h3 className = "m-3">Your Cart is Empty</h3>
	        <h5>Check our products <Link to = '/catalogue'><Button variant="outline-success">here</Button></Link> !</h5>
	        </div>
	    	}
	      </Container>
	    </div>
	  );
}