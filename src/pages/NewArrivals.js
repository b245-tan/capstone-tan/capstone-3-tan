import { Container, Row } from "react-bootstrap";
import { useEffect, useState, Fragment } from "react";
// import Carousel from 'react-bootstrap/Carousel';


import NewArrivalsCarousel from "../components/NewArrivals/NewArrivalsCarousel"
import ScrollButton from '../components/ScrollButton/ScrollButton.js';


export default function NewArrivals() {

  const [products, setProducts] = useState([])

  
  useEffect(() =>{
    fetch(`${process.env.REACT_APP_API_URL}/product/allProducts`)
    .then(result => result.json())
    .then(data => {
      setProducts(data.reverse().map(product => {
        return(
          <NewArrivalsCarousel key = {product._id} productProp = {product}/>
        )
      }))
    })

  }, [])


  return(
      <Fragment>
      <h1 className = "text-center mt-5 pt-5">New Arrivals</h1>
        <Container className="fluid mb-5 pb-5 col-sm-10 col-md-8 col-xl-8">
          <Row className = "mt-5">
              {products}
          </Row>
        </Container>
        <ScrollButton />
      </Fragment>

  )

}