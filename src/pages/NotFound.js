import { Link } from "react-router-dom"
import {Container, Figure} from 'react-bootstrap'

export default function NotFound() {
	
	return(
		<Container className = "pt-5 mt-5 text-center">
		<div>
		<Figure className = "pt-4">
		<Figure.Image
		width={400}
		height={500}
		alt="171x180"
		src="https://thumbs.dreamstime.com/b/page-not-found-error-hand-drawn-vector-layout-template-broken-robot-your-website-projects-76842565.jpg"
		/>
		</Figure>
		<h1>Page Not Found</h1>
		<p>Go back to the <Link to = '/'>homepage</Link>.</p>
		</div>
		</Container>
	)
}