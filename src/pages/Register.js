import {Button, Form, Container, Row, Col} from 'react-bootstrap';
import {Fragment} from 'react';

import {useContext, useState, useEffect} from 'react'
import {Navigate, useNavigate} from "react-router-dom"
import Swal from 'sweetalert2'

import UserContext from '../UserContext.js'


export default function Register(){
		
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const [confirmPassword, setConfirmPassword] = useState("");

	const navigate = useNavigate();

	const {user, setUser} = useContext(UserContext)
	const [isActive, setIsActive] = useState(false)
	const [isTheSame, setisTheSame] = useState(false)

	useEffect(() => {
		if(email !== "" && password !== "" && confirmPassword !== "" && password === confirmPassword){
			setIsActive(true)
		}
		else {
			setIsActive(false)
		}
	}, [email, password, confirmPassword])


	useEffect(() => {
		if(password === confirmPassword){
			setisTheSame(true)
		}
		else {
			setisTheSame(false)
		}
	}, [password, confirmPassword])



	function register(event){
		event.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}/user/register`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(result => result.json())
		.then(data => {
			console.log(data)
			if(data === true) {
				Swal.fire({
					title: "Registration Successful!",
					icon: "success",
					text: "Log-in to access the shop",
					showConfirmButton: true,
					timer: 2000
				})

				navigate("/login")
			}
			else {

				Swal.fire({
					title: "Registration failed!",
					icon: "error",
					text: "Email Address already taken!",
					showConfirmButton: false,
					timer: 2000
				})
			}
		})
	}


	return(

		user ?
		<Navigate to = "/*"/>
		:
		<Container className="fluid pb-5 mb-5">
		<Fragment>
		<h1 className = "text-center mt-5 pt-5">Sign Up</h1>
		<Row>
			<Col className = "col-md-6 mx-auto">
				<Form className = 'mt-5' onSubmit = {event=> register(event)}>
				      <Form.Group className="mb-3" controlId="formBasicEmail">
				        <Form.Label>Email Address</Form.Label>
				        <Form.Control 
					        type="email" 
					        placeholder="Enter email" 
					        value ={email}
					        onChange = {event => setEmail(event.target.value)}
					        required
					        />
				        <Form.Text className="text-muted">
				          We'll never share your email with anyone else.
				        </Form.Text>
				      </Form.Group>

				      <Form.Group className="mb-3" controlId="formBasicPassword">
				        <Form.Label>Password</Form.Label>
				        <Form.Control 
					        type="password" 
					        placeholder="Password" 
					        value ={password}
					        onChange = {event => setPassword(event.target.value)}
					        required
					        />
				      </Form.Group>

				      <Form.Group className="mb-2" controlId="formBasicConfirmPassword">
				        <Form.Label>Confirm Password</Form.Label>
				        <Form.Control 
					        type="password" 
					        placeholder="Confirm your password" 
					        value ={confirmPassword}
					        onChange = {event => setConfirmPassword(event.target.value)}
					        required
					        />
				      </Form.Group>

				      {
						isTheSame ?
							<Form.Text className="text-muted">
							</Form.Text>
							:
							<Form.Text className="text-danger">
							Password does not match!
							</Form.Text>
						}
						<br/>
				      {
				      	isActive ?
				      	<Button variant="primary" type="submit">
				        Submit
				      </Button>
				      :
				      <Button variant="secondary" type="submit" disabled>
				        Submit
				      </Button>
				      }

				    </Form>
		    	</Col>
		    </Row>
		 </Fragment>
		 </Container>
		)
}